/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.midterm1;

import java.util.Scanner;

/**
 *
 * @author Nippon
 */
public class mid1 {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int[] arr = new int[10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = kb.nextInt();
        }
        System.out.println("Maximum Subarray sum of A is " + maxSubArraySum(arr));
    }

    public static int maxSubArraySum(int[] arr) {
        int size = arr.length;
        int max = Integer.MIN_VALUE, max_end = 0;

        for (int i = 0; i < size - 1; i++) {
            int currentsum = 0;
            for (int j = i; j <= size - 1; j++) {
                currentsum += arr[j];
                if (currentsum > max) {
                    max = currentsum;
                }
            }
        }
        return max;
    }
}
